from poetry_demo import demo
import sys

if __name__ == "__main__":
    sys.exit(demo.main())
