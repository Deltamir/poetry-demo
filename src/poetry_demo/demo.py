# -*- coding: utf-8 -*-
"""Example style docstrings.

This module demonstrates documentation.
Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.
"""

from typing import Union


def demo_multiply(a: Union[int, float], b: Union[int, float]) -> Union[int, float]:
    """Multiply to values.

    Args:
        a (Union[int, Real]): _description_
        b (Union[int, Real, Complex]): _description_

    Returns:
        Union[int, Real]: _description_
    """
    return a * b


def main() -> int:
    """Handle main call.

    Returns:
        int: Always 0
    """
    res = demo_multiply(2, 3)
    print(res)
    return 0
