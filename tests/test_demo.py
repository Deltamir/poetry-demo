"""Test module for demo.py."""

import runpy

from pytest import CaptureFixture, raises

from poetry_demo import demo


def test_demo_multiply() -> None:
    """Test the demo_multiply function."""
    result = demo.demo_multiply(2, 3)
    assert result == 6


def test_main(capfd: CaptureFixture[str]) -> None:
    """Test the main function."""
    res = demo.main()
    out, err = capfd.readouterr()
    assert out == "6\n"
    assert res == 0


def test_exit() -> None:
    """Test the sys.exit line for main calls."""
    with raises(SystemExit) as pytest_wrapped_e:
        runpy.run_module("poetry_demo.__main__", run_name="__main__")
    assert pytest_wrapped_e.type is SystemExit
    assert pytest_wrapped_e.value.code == 0
