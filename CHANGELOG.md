## [1.8.3](https://gitlab.com/Deltamir/poetry-demo/compare/1.8.2...1.8.3) (2025-02-14)


### Bug Fixes

* test ([d5519bc](https://gitlab.com/Deltamir/poetry-demo/commit/d5519bccbbaf521664ff939df9a6ff70a23d4e0e))

## [1.8.2](https://gitlab.com/Deltamir/poetry-demo/compare/1.8.1...1.8.2) (2025-02-14)


### Bug Fixes

* test rebase ([4a247bc](https://gitlab.com/Deltamir/poetry-demo/commit/4a247bc7d5f7c104be33a65fa9528923a248cfca))

## [1.8.1](https://gitlab.com/Deltamir/poetry-demo/compare/1.8.0...1.8.1) (2025-02-14)


### Bug Fixes

* test ([327a44b](https://gitlab.com/Deltamir/poetry-demo/commit/327a44be1fe7b9d13a2886400f945e77f421b8ca))
* test ([c556692](https://gitlab.com/Deltamir/poetry-demo/commit/c5566929be8b9fb66354bfeefde9e3e126dfba11))
* test ([f6daaa8](https://gitlab.com/Deltamir/poetry-demo/commit/f6daaa8bcb5091adde7564825a6ca5c482ecd0e7))
* test ([a78af75](https://gitlab.com/Deltamir/poetry-demo/commit/a78af75d78135e046a92031e3c87d1fed96da1ab))

## [1.8.1-rc.2](https://gitlab.com/Deltamir/poetry-demo/compare/1.8.1-rc.1...1.8.1-rc.2) (2025-02-14)


### Bug Fixes

* test ([327a44b](https://gitlab.com/Deltamir/poetry-demo/commit/327a44be1fe7b9d13a2886400f945e77f421b8ca))

## [1.8.1-rc.1](https://gitlab.com/Deltamir/poetry-demo/compare/1.8.0...1.8.1-rc.1) (2025-02-14)


### Bug Fixes

* test ([c556692](https://gitlab.com/Deltamir/poetry-demo/commit/c5566929be8b9fb66354bfeefde9e3e126dfba11))
* test ([f6daaa8](https://gitlab.com/Deltamir/poetry-demo/commit/f6daaa8bcb5091adde7564825a6ca5c482ecd0e7))
* test ([a78af75](https://gitlab.com/Deltamir/poetry-demo/commit/a78af75d78135e046a92031e3c87d1fed96da1ab))

# [1.8.0](https://gitlab.com/Deltamir/poetry-demo/compare/1.7.4...1.8.0) (2025-02-14)


### Bug Fixes

* bug in releaserc ([6140c06](https://gitlab.com/Deltamir/poetry-demo/commit/6140c0643c46790c2ea4aabd7b26e9c2540aff5b))
* **deps:** update dependency anchore/syft to v1.19.0 ([42006d5](https://gitlab.com/Deltamir/poetry-demo/commit/42006d5cfe9b468dccb9beba1e86fff60f27f3f1))
* **deps:** update dependency anchore/syft to v1.19.0 ([02265b8](https://gitlab.com/Deltamir/poetry-demo/commit/02265b8049d0949703ace4ff25aae5795adaee82))
* test ([93a4e7b](https://gitlab.com/Deltamir/poetry-demo/commit/93a4e7bab5548aa65eb3128cc9abad72f776c208))
* test ([597a493](https://gitlab.com/Deltamir/poetry-demo/commit/597a49386f5250a48629b9c6f43d0d17b7068148))
* test ([2ef8cd5](https://gitlab.com/Deltamir/poetry-demo/commit/2ef8cd50c1e42d06789cb66965da0a309e4b2aeb))


### Features

* **deps:** update node.js to v22 (major) ([a040cdf](https://gitlab.com/Deltamir/poetry-demo/commit/a040cdfe3995c6a19839bf0d983bee0cf42de10a))

# [1.8.0-rc.2](https://gitlab.com/Deltamir/poetry-demo/compare/1.8.0-rc.1...1.8.0-rc.2) (2025-02-14)


### Bug Fixes

* test ([93a4e7b](https://gitlab.com/Deltamir/poetry-demo/commit/93a4e7bab5548aa65eb3128cc9abad72f776c208))
* test ([597a493](https://gitlab.com/Deltamir/poetry-demo/commit/597a49386f5250a48629b9c6f43d0d17b7068148))
* test ([2ef8cd5](https://gitlab.com/Deltamir/poetry-demo/commit/2ef8cd50c1e42d06789cb66965da0a309e4b2aeb))

# [1.8.0-rc.1](https://gitlab.com/Deltamir/poetry-demo/compare/1.7.5-rc.3...1.8.0-rc.1) (2025-02-13)


### Features

* **deps:** update node.js to v22 (major) ([a040cdf](https://gitlab.com/Deltamir/poetry-demo/commit/a040cdfe3995c6a19839bf0d983bee0cf42de10a))

## [1.7.5-rc.3](https://gitlab.com/Deltamir/poetry-demo/compare/1.7.5-rc.2...1.7.5-rc.3) (2025-02-13)


### Bug Fixes

* bug in releaserc ([6140c06](https://gitlab.com/Deltamir/poetry-demo/commit/6140c0643c46790c2ea4aabd7b26e9c2540aff5b))

## [1.7.5-rc.2](https://gitlab.com/Deltamir/poetry-demo/compare/1.7.5-rc.1...1.7.5-rc.2) (2025-02-13)


### Bug Fixes

* **deps:** update dependency anchore/syft to v1.19.0 ([42006d5](https://gitlab.com/Deltamir/poetry-demo/commit/42006d5cfe9b468dccb9beba1e86fff60f27f3f1))

## [1.7.5-rc.1](https://gitlab.com/Deltamir/poetry-demo/compare/1.7.4...1.7.5-rc.1) (2025-02-13)


### Bug Fixes

* **deps:** update dependency anchore/syft to v1.19.0 ([02265b8](https://gitlab.com/Deltamir/poetry-demo/commit/02265b8049d0949703ace4ff25aae5795adaee82))

## [1.7.5-beta.1](https://gitlab.com/Deltamir/poetry-demo/compare/1.7.4...1.7.5-beta.1) (2025-02-13)


### Bug Fixes

* **deps:** update dependency anchore/syft to v1.19.0 ([02265b8](https://gitlab.com/Deltamir/poetry-demo/commit/02265b8049d0949703ace4ff25aae5795adaee82))

## [1.7.4](https://gitlab.com/Deltamir/poetry-demo/compare/1.7.3...1.7.4) (2025-02-13)


### Bug Fixes

* final ([970acda](https://gitlab.com/Deltamir/poetry-demo/commit/970acdade6f7b5406939f4d9e934c5837958ca0c))
* test ([f2bc8ee](https://gitlab.com/Deltamir/poetry-demo/commit/f2bc8ee60c96cb6292a844a023a3cc72bd6dac8e))
* test with envsubstr ([a4a922b](https://gitlab.com/Deltamir/poetry-demo/commit/a4a922b9a1a9e1338138f34f167ad6741c14d7cb))
* typo ([4843ee7](https://gitlab.com/Deltamir/poetry-demo/commit/4843ee70da417ded7cac0c18c2ee3eadb83dbd24))

## [1.7.3](https://gitlab.com/Deltamir/poetry-demo/compare/1.7.2...1.7.3) (2025-02-13)


### Bug Fixes

* **deps:** update pre-commit hook astral-sh/uv-pre-commit to v0.5.31 ([ecbcb8d](https://gitlab.com/Deltamir/poetry-demo/commit/ecbcb8de80c8b6cdd7a86e97ef00e5cdd08d0d75))
* test ([7095998](https://gitlab.com/Deltamir/poetry-demo/commit/709599812aa2cd6bcecad26916577f0771c07e2f))
* Test ([18a7105](https://gitlab.com/Deltamir/poetry-demo/commit/18a7105d35cc71f002c5586563d405d5fcc1d69c))
* Test with src folder and typo in versioning template ([4b48410](https://gitlab.com/Deltamir/poetry-demo/commit/4b484101a8bfd4233adaca902f9b8d27a1a01ef6))

## [1.7.2](https://gitlab.com/Deltamir/poetry-demo/compare/1.7.1...1.7.2) (2025-02-13)


### Bug Fixes

* Incorrect versionning template ([69903d3](https://gitlab.com/Deltamir/poetry-demo/commit/69903d326c8a617afe16b4e4733ebd14be29c23e))

## [1.7.1](https://gitlab.com/Deltamir/poetry-demo/compare/1.7.0...1.7.1) (2025-02-13)


### Bug Fixes

* Mypy typo ([00cc31e](https://gitlab.com/Deltamir/poetry-demo/commit/00cc31ed7885b8f3f279559dfafa5874371794ba))
* test with renovate ([72c2fba](https://gitlab.com/Deltamir/poetry-demo/commit/72c2fbab29356e42cd7735922076de73687b4bd5))

# [1.7.0](https://gitlab.com/Deltamir/poetry-demo/compare/1.6.0...1.7.0) (2025-02-11)


### Features

* **deps:** update dependency sphinx to v8 (major) ([f6ac07e](https://gitlab.com/Deltamir/poetry-demo/commit/f6ac07e347e16ab9132b8ff8003679e85381173d))

# [1.6.0](https://gitlab.com/Deltamir/poetry-demo/compare/1.5.0...1.6.0) (2025-02-11)


### Features

* **deps:** update dependency pytest-cov to v6 (major) ([885cd6d](https://gitlab.com/Deltamir/poetry-demo/commit/885cd6dcd5c8c1561638e48106e325a59da9b038))

# [1.5.0](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.80...1.5.0) (2025-02-11)


### Features

* **deps:** update dependency pre-commit to v4 (major) ([e4bfd8d](https://gitlab.com/Deltamir/poetry-demo/commit/e4bfd8df642f4d7282bf3a1d7f01689c06b691b9))
* **deps:** update dependency sphinx-rtd-theme to v3 (major) ([d475a62](https://gitlab.com/Deltamir/poetry-demo/commit/d475a622db50878e50774bd30d03446e5f266d10))
* **deps:** update node.js to v22 (major) ([41b2ab1](https://gitlab.com/Deltamir/poetry-demo/commit/41b2ab11529fce156730111af961128bc7ade1ff))
* **deps:** update pre-commit hook pre-commit/pre-commit-hooks to v5 (major) ([1557d3e](https://gitlab.com/Deltamir/poetry-demo/commit/1557d3ee77e9bdcbc741f1a5d3d56dad6e5673ef))

## [1.4.80](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.79...1.4.80) (2025-02-11)


### Bug Fixes

* **deps:** update dependency ruff to v0.9.6 ([46e9197](https://gitlab.com/Deltamir/poetry-demo/commit/46e9197c45e216a7fb3d981235d9429bd1f218de))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.9.6 ([1a08dad](https://gitlab.com/Deltamir/poetry-demo/commit/1a08dad38accdd259eaba62b2347e41aee46061f))

## [1.4.79](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.78...1.4.79) (2025-02-07)


### Bug Fixes

* **deps:** update dependency ruff to v0.9.5 ([b4723e6](https://gitlab.com/Deltamir/poetry-demo/commit/b4723e6cc60c1662a597117120b4be09f1fbfff7))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.9.5 ([bfae440](https://gitlab.com/Deltamir/poetry-demo/commit/bfae440a2351a3ec34ebb2e878f0e900746381df))

## [1.4.78](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.77...1.4.78) (2025-02-06)


### Bug Fixes

* **deps:** update dependency mypy to ~1.15.0 ([c37270a](https://gitlab.com/Deltamir/poetry-demo/commit/c37270a2c8537a1961831f69c7fbb2e48dab2f0e))
* **deps:** update pre-commit hook pre-commit/mirrors-mypy to v1.15.0 ([a28bbbf](https://gitlab.com/Deltamir/poetry-demo/commit/a28bbbf02d29c9ff11c76a7028c86332e0c4212f))

## [1.4.77](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.76...1.4.77) (2025-01-31)


### Bug Fixes

* **deps:** update dependency ruff to v0.9.4 ([8198e5c](https://gitlab.com/Deltamir/poetry-demo/commit/8198e5cb8dea9f3389106b386b045762a9ca7a88))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.9.4 ([12575f1](https://gitlab.com/Deltamir/poetry-demo/commit/12575f18c8f299a2fa06b3e51093ab10bd148c69))

## [1.4.76](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.75...1.4.76) (2025-01-30)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.23.3 ([78457d5](https://gitlab.com/Deltamir/poetry-demo/commit/78457d5cb4fae50d2de226ccb297ce1a8eb3b612))

## [1.4.75](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.74...1.4.75) (2025-01-25)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.23.2 ([58e26bf](https://gitlab.com/Deltamir/poetry-demo/commit/58e26bfe385a6d82f9b25e7230aec570137f4faf))

## [1.4.74](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.73...1.4.74) (2025-01-24)


### Bug Fixes

* **deps:** update dependency ruff to v0.9.3 ([e09f682](https://gitlab.com/Deltamir/poetry-demo/commit/e09f682f03320f4f22c2b47c09f12b800c2bfda0))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.9.3 ([47577b2](https://gitlab.com/Deltamir/poetry-demo/commit/47577b2d66177bd6220432f6fb7fcae3534ffcb0))

## [1.4.73](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.72...1.4.73) (2025-01-19)


### Bug Fixes

* **deps:** update dependency numpy to v2.2.2 ([176145f](https://gitlab.com/Deltamir/poetry-demo/commit/176145fc8dcda1cc96a9f0bf4946abc7b4aa8121))

## [1.4.72](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.71...1.4.72) (2025-01-17)


### Bug Fixes

* **deps:** update dependency ruff to v0.9.2 ([f2da9b6](https://gitlab.com/Deltamir/poetry-demo/commit/f2da9b6626f6b1c04a479a413c2ef4333a3dd0cd))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.9.2 ([2b62c09](https://gitlab.com/Deltamir/poetry-demo/commit/2b62c09b86b6cd4c4775d81885b44942b60e01fa))

## [1.4.71](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.70...1.4.71) (2025-01-16)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.23.1 ([e0c98a8](https://gitlab.com/Deltamir/poetry-demo/commit/e0c98a86e255219177a80611f104b30332470679))

## [1.4.70](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.69...1.4.70) (2025-01-15)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.23.0 ([af68fbf](https://gitlab.com/Deltamir/poetry-demo/commit/af68fbf79a008ba385e1c9d963fd7dc203472bc0))

## [1.4.69](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.68...1.4.69) (2025-01-13)


### Bug Fixes

* **deps:** update dependency bandit to v1.8.2 ([76c42ef](https://gitlab.com/Deltamir/poetry-demo/commit/76c42efc6be98e066f05975527bc4053030060c7))
* **deps:** update pre-commit hook pycqa/bandit to v1.8.2 ([c37cd40](https://gitlab.com/Deltamir/poetry-demo/commit/c37cd401bac3835e45b5f03f8c3fbb3b6cb2498c))

## [1.4.68](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.67...1.4.68) (2025-01-11)


### Bug Fixes

* **deps:** update dependency ruff to v0.9.1 ([211b6be](https://gitlab.com/Deltamir/poetry-demo/commit/211b6be7e27e4354006e4635dd161d35a64aacbc))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.9.1 ([632d801](https://gitlab.com/Deltamir/poetry-demo/commit/632d801e7e36669d397ea7b84d8ea82d94177492))

## [1.4.67](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.66...1.4.67) (2025-01-10)


### Bug Fixes

* **deps:** update dependency ruff to ^0.9.0 ([385aa77](https://gitlab.com/Deltamir/poetry-demo/commit/385aa77189414b12d1745e349512692607d78f3f))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.9.0 ([df1c1fd](https://gitlab.com/Deltamir/poetry-demo/commit/df1c1fdaf7d6f445c1d5b1b87b33e59dd3a58665))

## [1.4.66](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.65...1.4.66) (2025-01-05)


### Bug Fixes

* **deps:** update dependency ruff to v0.8.6 ([6bab632](https://gitlab.com/Deltamir/poetry-demo/commit/6bab6327ff0df37e98598b512b19e4c262c15c34))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.8.6 ([d7aaede](https://gitlab.com/Deltamir/poetry-demo/commit/d7aaedefcd8090f9824eab495b632f54c3703741))

## [1.4.65](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.64...1.4.65) (2025-01-03)


### Bug Fixes

* **deps:** update dependency ruff to v0.8.5 ([4c92332](https://gitlab.com/Deltamir/poetry-demo/commit/4c92332daea104b57286ac99b079ccfe2af72fc0))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.8.5 ([bf0935b](https://gitlab.com/Deltamir/poetry-demo/commit/bf0935bdc49f9f2eb5769910df1a7b6a41af037a))

## [1.4.64](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.63...1.4.64) (2025-01-01)


### Bug Fixes

* **deps:** update pre-commit hook pre-commit/mirrors-mypy to v1.14.1 ([46595f4](https://gitlab.com/Deltamir/poetry-demo/commit/46595f48cab181f6ad09a3186be2c069d691e228))

## [1.4.63](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.62...1.4.63) (2024-12-31)


### Bug Fixes

* **deps:** update dependency mypy to v1.14.1 ([c3a7a73](https://gitlab.com/Deltamir/poetry-demo/commit/c3a7a73452f18564168d8c59ae18b369b358b4c0))
* **deps:** update pre-commit hook gitleaks/gitleaks to v8.22.1 ([a5a3a7b](https://gitlab.com/Deltamir/poetry-demo/commit/a5a3a7b09ba7e2e6b2bc8cb93199d82f8b59b483))

## [1.4.62](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.61...1.4.62) (2024-12-22)


### Bug Fixes

* **deps:** update dependency numpy to v2.2.1 ([c855093](https://gitlab.com/Deltamir/poetry-demo/commit/c855093f868c6bde67b909e3f64d830dfb9480a9))
* **deps:** update pre-commit hook pre-commit/mirrors-mypy to v1.14.0 ([1bdf575](https://gitlab.com/Deltamir/poetry-demo/commit/1bdf575ecd052f73128d082a45a2f47dfb3d5bbc))

## [1.4.61](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.60...1.4.61) (2024-12-21)


### Bug Fixes

* **deps:** update dependency mypy to ~1.14.0 ([96a4b08](https://gitlab.com/Deltamir/poetry-demo/commit/96a4b08c84a6a47dfe7d32a5acd2eece7df40634))
* **deps:** update pre-commit hook gitleaks/gitleaks to v8.22.0 ([8269012](https://gitlab.com/Deltamir/poetry-demo/commit/8269012ec8e0c019a85a9f6694e1db758a7d82c9))

## [1.4.60](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.59...1.4.60) (2024-12-20)


### Bug Fixes

* **deps:** update dependency ruff to v0.8.4 ([02d5ddb](https://gitlab.com/Deltamir/poetry-demo/commit/02d5ddb19e9f4f433ea89284576b9eccc6183c87))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.8.4 ([ec2075f](https://gitlab.com/Deltamir/poetry-demo/commit/ec2075f86dff5be0903dcb6f6e25ad753aad9334))

## [1.4.59](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.58...1.4.59) (2024-12-13)


### Bug Fixes

* **deps:** update dependency ruff to v0.8.3 ([acf68f8](https://gitlab.com/Deltamir/poetry-demo/commit/acf68f8ce7a3ec837706d12ed7cce0685859c131))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.8.3 ([1cfab42](https://gitlab.com/Deltamir/poetry-demo/commit/1cfab42b43800ce77f95dcc6b3a04aeaefe75c2c))

## [1.4.58](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.57...1.4.58) (2024-12-09)


### Bug Fixes

* **deps:** update dependency numpy to v2.2.0 ([6bbc22c](https://gitlab.com/Deltamir/poetry-demo/commit/6bbc22cb2f5eec0a9bd8be22afc65379eb0a5624))

## [1.4.57](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.56...1.4.57) (2024-12-06)


### Bug Fixes

* **deps:** update dependency ruff to v0.8.2 ([1031319](https://gitlab.com/Deltamir/poetry-demo/commit/1031319ae2dfa36cda45ffe1604bc6b71f0d840b))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.8.2 ([b0154a4](https://gitlab.com/Deltamir/poetry-demo/commit/b0154a46336345fa5dc8e5f247822486e11ac51d))

## [1.4.56](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.55...1.4.56) (2024-12-02)


### Bug Fixes

* **deps:** update dependency pytest to v8.3.4 ([285a9c2](https://gitlab.com/Deltamir/poetry-demo/commit/285a9c25cc021d766eafb707d7d42665a0d06654))

## [1.4.55](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.54...1.4.55) (2024-11-30)


### Bug Fixes

* **deps:** update dependency ruff to v0.8.1 ([3fd1a3a](https://gitlab.com/Deltamir/poetry-demo/commit/3fd1a3ad71bc41cceec098666a92dd278346588e))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.8.1 ([f4a5a22](https://gitlab.com/Deltamir/poetry-demo/commit/f4a5a22aab6e555c544e90c05e92dda0527900d3))

## [1.4.54](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.53...1.4.54) (2024-11-27)


### Bug Fixes

* **deps:** update dependency bandit to ~1.8.0 ([bbf6195](https://gitlab.com/Deltamir/poetry-demo/commit/bbf6195721b13e999f82cfbe4f051d5f8a211395))
* **deps:** update pre-commit hook pycqa/bandit to v1.8.0 ([2693264](https://gitlab.com/Deltamir/poetry-demo/commit/26932648ece73a2e618be1ae1425ea2decf451f9))

## [1.4.53](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.52...1.4.53) (2024-11-23)


### Bug Fixes

* **deps:** update dependency ruff to ^0.8.0 ([2f19895](https://gitlab.com/Deltamir/poetry-demo/commit/2f19895f11e4b5f0bcf68cf9c3db3b9d90086259))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.8.0 ([b0653ad](https://gitlab.com/Deltamir/poetry-demo/commit/b0653ad75e65aaf86c6d3b89798febb4bb1c144d))

## [1.4.52](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.51...1.4.52) (2024-11-16)


### Bug Fixes

* **deps:** update dependency ruff to v0.7.4 ([1451918](https://gitlab.com/Deltamir/poetry-demo/commit/145191808967dbb3ef6e818daa6cb60fbd065eab))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.7.4 ([1d8d3ed](https://gitlab.com/Deltamir/poetry-demo/commit/1d8d3ed3bf837401fe6adb359344673f55e6df65))

## [1.4.51](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.50...1.4.51) (2024-11-09)


### Bug Fixes

* **deps:** update dependency ruff to v0.7.3 ([310f7eb](https://gitlab.com/Deltamir/poetry-demo/commit/310f7ebee2c193c5e5592e4c7df389ef49383cc9))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.7.3 ([06feec5](https://gitlab.com/Deltamir/poetry-demo/commit/06feec5716e38ed5ca8fd3e584875b5e29901dfd))

## [1.4.50](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.49...1.4.50) (2024-11-03)


### Bug Fixes

* **deps:** update dependency numpy to v2.1.3 ([b508f32](https://gitlab.com/Deltamir/poetry-demo/commit/b508f32aa43bd3d14f519566a0b79b419ee71029))

## [1.4.49](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.48...1.4.49) (2024-11-02)


### Bug Fixes

* **deps:** update dependency ruff to v0.7.2 ([dc220d4](https://gitlab.com/Deltamir/poetry-demo/commit/dc220d4284ae0d2112fd9c83486e3ae4a39f9dc8))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.7.2 ([d5433a7](https://gitlab.com/Deltamir/poetry-demo/commit/d5433a7c5bf35b23e2ebee3578d42bd010b736e5))

## [1.4.48](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.47...1.4.48) (2024-10-29)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.21.2 ([337390a](https://gitlab.com/Deltamir/poetry-demo/commit/337390a527153f57abfe5422419ec9ed4a28f91b))

## [1.4.47](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.46...1.4.47) (2024-10-25)


### Bug Fixes

* **deps:** update dependency ruff to v0.7.1 ([6d79155](https://gitlab.com/Deltamir/poetry-demo/commit/6d79155a40a08f118d3ab54852c39d724aff89ac))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.7.1 ([1a8b71a](https://gitlab.com/Deltamir/poetry-demo/commit/1a8b71a7cf1023f1f4f3944d055140dbf5930cd4))

## [1.4.46](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.45...1.4.46) (2024-10-24)


### Bug Fixes

* **deps:** update pre-commit hook pre-commit/mirrors-mypy to v1.13.0 ([0c369d4](https://gitlab.com/Deltamir/poetry-demo/commit/0c369d46c3841bf00093b3847cecea4ea16316d9))

## [1.4.45](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.44...1.4.45) (2024-10-23)


### Bug Fixes

* **deps:** update dependency mypy to ~1.13.0 ([259b143](https://gitlab.com/Deltamir/poetry-demo/commit/259b143888777e61e2e9200400e85c02d22638c6))
* **deps:** update pre-commit hook pre-commit/mirrors-mypy to v1.12.1 ([468c5dd](https://gitlab.com/Deltamir/poetry-demo/commit/468c5ddb94340fcb9ce4688636affbb950aace26))

## [1.4.44](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.43...1.4.44) (2024-10-20)


### Bug Fixes

* **deps:** update dependency mypy to v1.12.1 ([96aaa1f](https://gitlab.com/Deltamir/poetry-demo/commit/96aaa1f8e22fddd8f91fddae49d2a5b1af2fba02))

## [1.4.43](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.42...1.4.43) (2024-10-19)


### Bug Fixes

* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.7.0 ([271ece7](https://gitlab.com/Deltamir/poetry-demo/commit/271ece7f6cacb91cbbb7e9f9d134ed77cedcb422))
* **deps:** update pre-commit hook gitleaks/gitleaks to v8.21.1 ([aa8cde3](https://gitlab.com/Deltamir/poetry-demo/commit/aa8cde32f7abee087b0ca9e24674f13035dbeaf1))

## [1.4.42](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.41...1.4.42) (2024-10-18)


### Bug Fixes

* **deps:** update dependency ruff to ^0.7.0 ([e96659a](https://gitlab.com/Deltamir/poetry-demo/commit/e96659aa3e0eff7c3e8c7011ed296cbd6884dbd8))

## [1.4.41](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.40...1.4.41) (2024-10-16)


### Bug Fixes

* **deps:** update pre-commit hook pre-commit/mirrors-mypy to v1.12.0 ([4c6b3cf](https://gitlab.com/Deltamir/poetry-demo/commit/4c6b3cf22bd943982d101eb3d7b267d3e176ff6f))

## [1.4.40](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.39...1.4.40) (2024-10-15)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.21.0 ([bf7762b](https://gitlab.com/Deltamir/poetry-demo/commit/bf7762b2f2aa89755e383415276a3f3f5427a54a))

## [1.4.39](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.38...1.4.39) (2024-10-15)


### Bug Fixes

* **deps:** update dependency mypy to ~1.12.0 ([11b774b](https://gitlab.com/Deltamir/poetry-demo/commit/11b774b4ac84f776bf20d0e49b8756f57242025b))

## [1.4.38](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.37...1.4.38) (2024-10-09)


### Bug Fixes

* **deps:** update python ([e09169b](https://gitlab.com/Deltamir/poetry-demo/commit/e09169bfd537e35290f934310364e3096de4c7f0))

## [1.4.37](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.36...1.4.37) (2024-10-09)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.20.1 ([3f8dca7](https://gitlab.com/Deltamir/poetry-demo/commit/3f8dca7b89fbc1c0d1fcc5a62465b79edf1a405e))

## [1.4.36](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.35...1.4.36) (2024-10-06)


### Bug Fixes

* **deps:** update dependency numpy to v2.1.2 ([a40ecda](https://gitlab.com/Deltamir/poetry-demo/commit/a40ecda2acc83358d73e49b6b020ea69d400a4c7))

## [1.4.35](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.34...1.4.35) (2024-10-05)


### Bug Fixes

* **deps:** update dependency ruff to v0.6.9 ([642c095](https://gitlab.com/Deltamir/poetry-demo/commit/642c0959ac9e45f73e1c1355692a86891a1cb0ea))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.6.9 ([228f124](https://gitlab.com/Deltamir/poetry-demo/commit/228f12422716d176c76d8dbbbee29a8a220f5747))

## [1.4.34](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.33...1.4.34) (2024-10-04)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.20.0 ([6990326](https://gitlab.com/Deltamir/poetry-demo/commit/6990326d8dbe6852d26de9144e590e7774784022))

## [1.4.33](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.32...1.4.33) (2024-09-28)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.19.3 ([75eb2b5](https://gitlab.com/Deltamir/poetry-demo/commit/75eb2b560c91daf97f9a0e87635217c02e8587b6))

## [1.4.32](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.31...1.4.32) (2024-09-27)


### Bug Fixes

* **deps:** update dependency ruff to v0.6.8 ([7972ab9](https://gitlab.com/Deltamir/poetry-demo/commit/7972ab9789d5f1df3927b223980a5cf26cbe0de5))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.6.8 ([406ded5](https://gitlab.com/Deltamir/poetry-demo/commit/406ded59f9c68883871b2975e75e38aa3fe0eee5))

## [1.4.31](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.30...1.4.31) (2024-09-24)


### Bug Fixes

* **deps:** update dependency bandit to v1.7.10 ([dd77197](https://gitlab.com/Deltamir/poetry-demo/commit/dd771975cb947e061da8b855cc3faf5d7611d8bd))
* **deps:** update pre-commit hook pycqa/bandit to v1.7.10 ([d815c3a](https://gitlab.com/Deltamir/poetry-demo/commit/d815c3abdaf7ce142050f2361238a38c0f7c851e))

## [1.4.30](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.29...1.4.30) (2024-09-22)


### Bug Fixes

* **deps:** update dependency ruff to v0.6.7 ([70c7f8c](https://gitlab.com/Deltamir/poetry-demo/commit/70c7f8c3ac6d4011488da47dc9dbaad2e5abf6fa))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.6.7 ([0edf900](https://gitlab.com/Deltamir/poetry-demo/commit/0edf90071af6b3ae6507972d85404c1a28a3f068))

## [1.4.29](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.28...1.4.29) (2024-09-21)


### Bug Fixes

* **deps:** update dependency ruff to v0.6.6 ([0f20951](https://gitlab.com/Deltamir/poetry-demo/commit/0f20951d1486fb13f38f49f6da519978a315c334))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.6.6 ([19bad38](https://gitlab.com/Deltamir/poetry-demo/commit/19bad38db5b08d0b2d5c386a46a9d9d1bb48a198))

## [1.4.28](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.27...1.4.28) (2024-09-17)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.19.2 ([9a847b5](https://gitlab.com/Deltamir/poetry-demo/commit/9a847b5ae9404b5f990a4a0170648d7618b4fd09))

## [1.4.27](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.26...1.4.27) (2024-09-15)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.19.1 ([70c5a51](https://gitlab.com/Deltamir/poetry-demo/commit/70c5a51d7f33cb15edaff86f8f090e864e3ebc05))

## [1.4.26](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.25...1.4.26) (2024-09-14)


### Bug Fixes

* **deps:** update dependency ruff to v0.6.5 ([4026b7a](https://gitlab.com/Deltamir/poetry-demo/commit/4026b7a5888a44f20246802f13efb2c946c387d6))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.6.5 ([ce50da8](https://gitlab.com/Deltamir/poetry-demo/commit/ce50da8d66dc54193359d3cdbfb0e3e01da12b67))

## [1.4.25](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.24...1.4.25) (2024-09-11)


### Bug Fixes

* **deps:** update dependency pytest to v8.3.3 ([2b4e94d](https://gitlab.com/Deltamir/poetry-demo/commit/2b4e94d35fa6088d15f7882e8909a080af5c08e3))

## [1.4.24](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.23...1.4.24) (2024-09-06)


### Bug Fixes

* **deps:** update dependency ruff to v0.6.4 ([991ce83](https://gitlab.com/Deltamir/poetry-demo/commit/991ce83aef8efdb59ecf88090d11f266b855d4f3))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.6.4 ([9d71293](https://gitlab.com/Deltamir/poetry-demo/commit/9d712930adaa55c36d12646e63282cfd42c5d6d3))

## [1.4.23](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.22...1.4.23) (2024-09-04)


### Bug Fixes

* **deps:** update dependency numpy to v2.1.1 ([8438de7](https://gitlab.com/Deltamir/poetry-demo/commit/8438de773505abc211298d8779a5b5128c22ce10))

## [1.4.22](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.21...1.4.22) (2024-08-30)


### Bug Fixes

* **deps:** update dependency ruff to v0.6.3 ([45955f5](https://gitlab.com/Deltamir/poetry-demo/commit/45955f523cfead4ec158f60aa33af465287feb05))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.6.3 ([be990c7](https://gitlab.com/Deltamir/poetry-demo/commit/be990c7ba833c53a7a29a20400fa08306836ea51))

## [1.4.21](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.20...1.4.21) (2024-08-26)


### Bug Fixes

* **deps:** update pre-commit hook pre-commit/mirrors-mypy to v1.11.2 ([381a893](https://gitlab.com/Deltamir/poetry-demo/commit/381a893de7815ffcca1205aac879708d040391f8))

## [1.4.20](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.19...1.4.20) (2024-08-25)


### Bug Fixes

* **deps:** update dependency mypy to v1.11.2 ([b1678ec](https://gitlab.com/Deltamir/poetry-demo/commit/b1678ecb4dca482af57effb32b0f24936c6be4e2))

## [1.4.19](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.18...1.4.19) (2024-08-23)


### Bug Fixes

* **deps:** update dependency ruff to v0.6.2 ([21fe4bc](https://gitlab.com/Deltamir/poetry-demo/commit/21fe4bc3e73a1ae35aedf02981d547b5059e43e4))
* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.6.2 ([aad2adf](https://gitlab.com/Deltamir/poetry-demo/commit/aad2adf30fe3c7c4d030c7929e6f2d5e46df72a5))

## [1.4.18](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.17...1.4.18) (2024-08-21)


### Bug Fixes

* **deps:** update pre-commit hook astral-sh/ruff-pre-commit to v0.6.1 ([97920ea](https://gitlab.com/Deltamir/poetry-demo/commit/97920ea72e992a78859a10bb804c146365d5a116))
* **deps:** update pre-commit hook pre-commit/mirrors-mypy to v1.11.1 ([a24fda1](https://gitlab.com/Deltamir/poetry-demo/commit/a24fda1c8814c0a45dad3d6cccb59ee8e72a5ded))
* **deps:** update pre-commit hook pre-commit/pre-commit-hooks to v4.6.0 ([e87a672](https://gitlab.com/Deltamir/poetry-demo/commit/e87a672efec10d0f03648aaf2e6d7118bb279877))

## [1.4.17](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.16...1.4.17) (2024-08-21)


### Bug Fixes

* **deps:** update pre-commit hook gitleaks/gitleaks to v8.18.4 ([bafcb17](https://gitlab.com/Deltamir/poetry-demo/commit/bafcb17c8e60f706ada8d504350d6039db004e09))
* **deps:** update pre-commit hook pycqa/bandit to v1.7.9 ([660827e](https://gitlab.com/Deltamir/poetry-demo/commit/660827ea3356b4924c2c3132b7b4f1b57ebe1f31))

## [1.4.16](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.15...1.4.16) (2024-08-20)


### Bug Fixes

* Adding poetry lock ([b59989e](https://gitlab.com/Deltamir/poetry-demo/commit/b59989e5d9e1d364eba8345df051526e1482e002))

## [1.4.15](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.14...1.4.15) (2024-08-20)


### Bug Fixes

* Blanking path prefix to allow redeployment ([af1c4c8](https://gitlab.com/Deltamir/poetry-demo/commit/af1c4c88fcba7d71392bb591d270c0ba52ee4011))

## [1.4.14](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.13...1.4.14) (2024-08-12)


### Bug Fixes

* Test ([fcadf1c](https://gitlab.com/Deltamir/poetry-demo/commit/fcadf1ce4b98c9529af242a78b852f6ff8bf4581))

## [1.4.13](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.12...1.4.13) (2024-08-12)


### Bug Fixes

* Test ([28ca2ba](https://gitlab.com/Deltamir/poetry-demo/commit/28ca2ba49439d7f1d4e127ba380d61a240badbfc))

## [1.4.12](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.11...1.4.12) (2024-08-12)


### Bug Fixes

* new test ([82119db](https://gitlab.com/Deltamir/poetry-demo/commit/82119dba67493995ad7fb6da6db01f4af96683dc))

## [1.4.11](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.10...1.4.11) (2024-08-12)


### Bug Fixes

* Test change workflow ([62834ef](https://gitlab.com/Deltamir/poetry-demo/commit/62834efc8e321cdcb8db376c8bb7afa677dcadc4))

## [1.4.10](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.9...1.4.10) (2024-08-12)


### Bug Fixes

* Change .releaserc ([995ada2](https://gitlab.com/Deltamir/poetry-demo/commit/995ada24587d8f714db33a6feaafe75d731bd480))

## [1.4.9](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.8...1.4.9) (2024-08-12)


### Bug Fixes

* Fix extend ([9c03b46](https://gitlab.com/Deltamir/poetry-demo/commit/9c03b4632b72ca01261fc187412cf552f324a991))
* Test with workflow ([43c833f](https://gitlab.com/Deltamir/poetry-demo/commit/43c833fb99c56ca6e8a31c0868e54b21d68c3eea))

## [1.4.8](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.7...1.4.8) (2024-08-12)


### Bug Fixes

* Adding a when always ([67f5830](https://gitlab.com/Deltamir/poetry-demo/commit/67f58302b77fbe5fb570c1e114f94645c1bcbc35))

## [1.4.7](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.6...1.4.7) (2024-08-12)


### Bug Fixes

* Again ([bb7a708](https://gitlab.com/Deltamir/poetry-demo/commit/bb7a708fd70c3933013730913e027605bdde85e1))
* Try again ([b5a6215](https://gitlab.com/Deltamir/poetry-demo/commit/b5a621519ef80999754dffaeb5d5bba115deb795))

## [1.4.6](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.5...1.4.6) (2024-08-12)


### Bug Fixes

* Bug workflow ([56fdcbc](https://gitlab.com/Deltamir/poetry-demo/commit/56fdcbcede0665cb9d38863f00a3b503bbf537bf))
* Skip commit ref name ([64bebc5](https://gitlab.com/Deltamir/poetry-demo/commit/64bebc53f55f7065d50f1e03083fa3fb5508c0f3))
* Try by extending base job ([0d6ea88](https://gitlab.com/Deltamir/poetry-demo/commit/0d6ea8856c7e2be4ac689fcfc1f6458ca731910e))

## [1.4.5](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.4...1.4.5) (2024-08-12)


### Bug Fixes

* Add workflow ([257b531](https://gitlab.com/Deltamir/poetry-demo/commit/257b531f6ddee8b70360ac1696110b090890e16d))

## [1.4.4](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.3...1.4.4) (2024-08-12)


### Bug Fixes

* Rules bug ([5e297c4](https://gitlab.com/Deltamir/poetry-demo/commit/5e297c410dea8460930be7720ea47f1547c271cb))
* Try to skip ci on main branch ([85f5a21](https://gitlab.com/Deltamir/poetry-demo/commit/85f5a2120dcb29c10032911528ffe6ea5969a4ba))
* Try to skip ci on prod branches ([9255ccb](https://gitlab.com/Deltamir/poetry-demo/commit/9255ccb0119d876fe5c3d3f5f2c5c4b88351dd71))

## [1.4.3](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.2...1.4.3) (2024-08-12)


### Bug Fixes

* skip ci on main ([90c7149](https://gitlab.com/Deltamir/poetry-demo/commit/90c714964e4d3254f4860cf6bade408515f076cf))

## [1.4.2](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.1...1.4.2) (2024-08-12)


### Bug Fixes

* cyclonedx-bom output file and skip ci on prod ([4b0307e](https://gitlab.com/Deltamir/poetry-demo/commit/4b0307e171f0e9f666c842eb2f7831929df1b3da))

## [1.4.1](https://gitlab.com/Deltamir/poetry-demo/compare/1.4.0...1.4.1) (2024-08-12)


### Bug Fixes

* Trigger a test release ([280b239](https://gitlab.com/Deltamir/poetry-demo/commit/280b239e8cb5d97223070137586d4e81a8dfc3c5))

# [1.4.0](https://gitlab.com/Deltamir/poetry-demo/compare/1.3.1...1.4.0) (2024-08-02)


### Bug Fixes

* Cyclonedx CLI changes ([e099d33](https://gitlab.com/Deltamir/poetry-demo/commit/e099d33023c8796e2e7379f3ea879633dfd0dba8))


### Features

* Add devcontainers ([5d41be2](https://gitlab.com/Deltamir/poetry-demo/commit/5d41be20f709cb733954af30dfefb21b9105f965))
* Migrate to ruff ([268833e](https://gitlab.com/Deltamir/poetry-demo/commit/268833e1e23673391503606a4f2eeff382bead83))

## [1.3.1](https://gitlab.com/Deltamir/poetry-demo/compare/1.3.0...1.3.1) (2024-1-28)


### Bug Fixes

* **semrel:** Flag beta as a prerelease branche ([a4728d3](https://gitlab.com/Deltamir/poetry-demo/commit/a4728d31be53b6af0c8c7810668ab6b45c426862))


### Reverts

* **semrel:** Revert back to angular commit convention ([bb637df](https://gitlab.com/Deltamir/poetry-demo/commit/bb637df0977100fd3f422747d631b650695af6aa))

## [1.3.1-beta.1](https://gitlab.com/Deltamir/poetry-demo/compare/1.3.0...1.3.1-beta.1) (2024-1-28)


### Bug Fixes

* **semrel:** Flag beta as a prerelease branche ([a4728d3](https://gitlab.com/Deltamir/poetry-demo/commit/a4728d31be53b6af0c8c7810668ab6b45c426862))

# [1.2.0](https://gitlab.com/Deltamir/poetry-demo/compare/1.1.4...1.2.0) (2024-1-27)


### Features

* **semrel:** Migrate to conventionalcommits in semrel conf ([55a8200](https://gitlab.com/Deltamir/poetry-demo/commit/55a82002e2e4616f3b8e00ea96d090ea81b1c10e))

## [1.1.4](https://gitlab.com/Deltamir/poetry-demo/compare/1.1.3...1.1.4) (2024-1-25)


### Bug Fixes

* **semrel:** Add type to python packages ([ffd349f](https://gitlab.com/Deltamir/poetry-demo/commit/ffd349fe55334e5709215d9e6a4492121115a610))

## [1.1.3](https://gitlab.com/Deltamir/poetry-demo/compare/v1.1.2...1.1.3) (2024-1-25)


### Bug Fixes

* **semrel:** Change tag format and gitlab release assets ([447ca15](https://gitlab.com/Deltamir/poetry-demo/commit/447ca15a6b20c2998561eb6644d21b570cf50268))

## [1.1.2](https://gitlab.com/Deltamir/poetry-demo/compare/v1.1.1...v1.1.2) (2024-1-25)


### Bug Fixes

* **semrel:** Add assets in gitlab release ([be163e8](https://gitlab.com/Deltamir/poetry-demo/commit/be163e837534b6c8ff48c75394090ae2e6350395))

## [1.1.1](https://gitlab.com/Deltamir/poetry-demo/compare/v1.1.0...v1.1.1) (2024-1-23)


### Bug Fixes

* **deps:** update python ([e93f792](https://gitlab.com/Deltamir/poetry-demo/commit/e93f79258be1c1b315063973e7bb7b9480c08c4f))

# [1.1.0](https://gitlab.com/Deltamir/poetry-demo/compare/v1.0.0...v1.1.0) (2024-1-23)


### Bug Fixes

* **semrel:** adding a python-venv dependency ([85ccecb](https://gitlab.com/Deltamir/poetry-demo/commit/85ccecbe77dbfc6ef8751c7dd1149becd1001892))
* **semrel:** installing python dep ([6981a27](https://gitlab.com/Deltamir/poetry-demo/commit/6981a2765b917e06fa01f1407605884048a6fb90))
* **semrel:** typo in .releaserc ([7deb021](https://gitlab.com/Deltamir/poetry-demo/commit/7deb0216fef09448f27706c06cc2c58613bd52be))
* **semrel:** typo in gitlab_ci.yml ([ebdfd39](https://gitlab.com/Deltamir/poetry-demo/commit/ebdfd39c138084b38d8e40bfa66b76daf23e0e05))
* **semrel:** typo in npm package ([4db5f45](https://gitlab.com/Deltamir/poetry-demo/commit/4db5f45e8e809c266e81af1746ffefc28740bb26))


### Features

* **semrel:** first try with pypi and git plugins ([dd55551](https://gitlab.com/Deltamir/poetry-demo/commit/dd55551c774e9be0d35fd7f83833fba39c033553))
