poetry\_demo package
====================

Submodules
----------

poetry\_demo.demo module
------------------------

.. automodule:: poetry_demo.demo
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: poetry_demo
   :members:
   :undoc-members:
   :show-inheritance:
